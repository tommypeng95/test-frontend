import React from "react";
import { useSearchParams } from "react-router-dom";

export function useGetSearchParams() {
  const [searchParams, setSearchParams] = useSearchParams({ subscription: false });

  const getSearchParamsObject = () => {
    let obj = {};
    for (const entry of searchParams.entries()) {
      const [param, value] = entry;
      obj[param] = value;
    }
    return obj;
  };

  return [getSearchParamsObject(), setSearchParams, searchParams];
}
