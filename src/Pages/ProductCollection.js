import React from "react";
import { Box } from "@material-ui/core";
import debounce from "lodash.debounce";

import PageLayout from "../Components/PageLayout";
import ProductCollectionTable from "../Components/Product.Collection.Table";
import Sidebar from "../Components/Sidebar";

import { useGetSearchParams } from "../Hooks/useGetSearchParams";

import searchProducts from "../Api/Calls/Products/search";

import { arrayReducer, actions } from "../Reducers/arrayReducer";

const debouncedFetchProductData = debounce((query, cb) => {
  fetchProductData(query, cb);
}, 500);

const fetchProductData = async (query, cb) => {
  const res = await searchProducts(query);
  cb(res);
};

export default function ProductCollectionPage() {
  const [productData, productDataDispatch] = React.useReducer(arrayReducer, []);
  const [dataCount, setDataCount] = React.useState(0);

  const [isLoading, setIsLoading] = React.useState(true);

  //state for pagination fetching
  const [isLoadingMore, setIsLoadingMore] = React.useState(false);
  const [pageCursor, setPageCursor] = React.useState(1);

  const [searchParams, setSearchParams] = useGetSearchParams();

  const fetchProductsCallBack = (res) => {
    productDataDispatch(actions.overwrite(res.data));
    setDataCount(res.count);
    setIsLoading(false);
    setIsLoadingMore(false);

    //set page cursor to second page
    setPageCursor(2);
  };

  const _handleSetSearchParams = (key, value) => {
    //if new parameters are set, call new search starting at page one.
    const newSearchParams = { ...searchParams, ...{ [key]: value, page: 1 } };
    setSearchParams(newSearchParams);
    setIsLoading(true);
    debouncedFetchProductData(newSearchParams, fetchProductsCallBack);
  };

  React.useEffect(() => {
    fetchProductData({ ...searchParams, page: pageCursor }, fetchProductsCallBack);
  }, []);

  const _loadMore = () => {
    setIsLoadingMore(true);
    const loadMoreCallBack = (res) => {
      productDataDispatch(actions.pushBatch(res.data));
      setIsLoading(false);
      setPageCursor(pageCursor + 1);
      setIsLoadingMore(false);
    };
    debouncedFetchProductData({ ...searchParams, page: pageCursor }, loadMoreCallBack);
  };

  return (
    <PageLayout>
      <Box display={"flex"} flexDirection={"row"}>
        <Sidebar filterValues={searchParams} setSearchParams={_handleSetSearchParams}></Sidebar>
        <ProductCollectionTable
          productData={productData}
          count={dataCount}
          isLoading={isLoading}
          loadMore={_loadMore}
          isLoadingMore={isLoadingMore}
        ></ProductCollectionTable>
      </Box>
    </PageLayout>
  );
}
