import { createTheme } from "@material-ui/core";

export const theme = createTheme({
  palette: {
    primary: {
      main: "#bc2e3e",
    },
    secondary: {
      main: "#001c72",
    },
  },
});
