function arrayReducer(state, action) {
  switch (action.type) {
    case "push":
      return [...state, action.data];

    case "pushBatch":
      return [...state, ...action.data];
    case "remove":
      return (state || []).filter((e) => e !== action.data);
    case "overwrite":
      return action.data;
    case "clear":
      return [];
    default:
      return state;
  }
}

export default arrayReducer;
