export const push = (data) => {
  return { type: "push", data };
};
export const pushBatch = (data) => {
  return { type: "pushBatch", data };
};
export const remove = (data) => {
  return { type: "remove", data };
};
export const clear = () => {
  return { type: "clear" };
};
export const overwrite = (data) => {
  return { type: "overwrite", data };
};

export const actions = { push, remove, clear, overwrite, pushBatch };
