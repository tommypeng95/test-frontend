import React from "react";
import logo from "./logo.svg";
import "./App.css";

import ProductCollectionPage from "./Pages/ProductCollection";
import { Routes, Route, Navigate } from "react-router-dom";

function App() {
  return (
    <Routes>
      <Route path="/" element={<ProductCollectionPage />}></Route>
      <Route path="*" element={<Navigate to="/" />} />
    </Routes>
  );
}

export default App;
