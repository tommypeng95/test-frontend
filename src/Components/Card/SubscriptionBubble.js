import { Box, makeStyles, Typography } from "@material-ui/core";
import React from "react";

const useStyles = makeStyles((theme) => ({
  root: {
    borderRadius: 8,
    padding: "4px 8px",
    position: "absolute",
    alignItems: "center",
    justifyContent: "center",
    display: "flex",
    backgroundColor: "#0000b9",
    color: "white",
  },
}));
export default function SubscriptionBubble({ children, hasSubscription, ...rest }) {
  const classes = useStyles();
  if (!hasSubscription) return <></>;
  return (
    <Box className={classes.root} {...rest}>
      <Typography>Subscribe and Save</Typography>
    </Box>
  );
}
