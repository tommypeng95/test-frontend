import { Box, makeStyles, Typography } from "@material-ui/core";
import React from "react";

const useStyles = makeStyles((theme) => ({ root: { fontSize: 20 } }));

export default function Title({ children }) {
  const classes = useStyles();
  return (
    <Typography className={classes.root} color={"secondary"}>
      {children}
    </Typography>
  );
}
