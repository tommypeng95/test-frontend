import { Typography } from "@material-ui/core";
import React from "react";

export default function Title({ children }) {
  return <Typography>{children.join(", ")}</Typography>;
}
