import { Box, Typography } from "@material-ui/core";
import React from "react";

export default function Thumbnail({ alt, src }) {
  return <img src={src} alt={alt} height={"400px"} loading={"lazy"}></img>;
}
