import { Button as MuiButton } from "@material-ui/core";
import React from "react";

export default function Button({ children, href }) {
  return (
    <MuiButton
      component={"a"}
      href={href}
      fullWidth
      variant="contained"
      color={"primary"}
      style={{ fontSize: 16, letterSpacing: "0.2rem" }}
    >
      {children}
    </MuiButton>
  );
}
