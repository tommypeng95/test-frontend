import { Box, Typography } from "@material-ui/core";
import React from "react";

export default function Content({ children }) {
  return (
    <Box width={"100%"} marginTop={"8px"}>
      {children}
    </Box>
  );
}
