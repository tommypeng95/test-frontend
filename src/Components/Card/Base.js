import { Box, makeStyles, Typography } from "@material-ui/core";
import React from "react";

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: "400px",
    [theme.breakpoints.down("md")]: {
      width: "calc(50% - 8px)",
    },
    [theme.breakpoints.up("md")]: {
      width: "calc(33.33% - 11px)",
    },
    [theme.breakpoints.up("lg")]: {
      width: "calc(25% - 12px);",
    },
  },
}));

export default function Base({ children }) {
  const classes = useStyles();
  return (
    <Box
      display={"flex"}
      flexDirection={"column"}
      className={classes.root}
      // border={"1px solid #efefef"}
      overflow={"hidden"}
      alignItems={"center"}
      marginBottom={"47px"}
      marginRight={"16px"}
      position={"relative"}
    >
      {children}
    </Box>
  );
}
