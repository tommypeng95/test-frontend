import { Typography } from "@material-ui/core";
import React from "react";

export default function Price({ children }) {
  return (
    <Typography color={"primary"} style={{ fontSize: 20 }}>
      From £{children}
    </Typography>
  );
}
