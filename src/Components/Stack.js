import { Box, Typography } from "@material-ui/core";
import React from "react";

export default function Stack({ children, width, ...rest }) {
  return (
    <Box display={"flex"} flexDirection={"column"} {...rest}>
      {children}
    </Box>
  );
}
