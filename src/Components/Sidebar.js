import { Box, Divider, FormControlLabel, Switch, TextField, Typography } from "@material-ui/core";
import React from "react";
import Stack from "./Stack";
import TextFilter from "./TextFilter";

export default function Sidebar({ filterValues, setSearchParams }) {
  const _handleChange = (id, value, e) => !!setSearchParams && setSearchParams(id, value, e);
  return (
    <Stack minWidth={"230px"} maxWidth={"230px"} marginRight={"25px"} paddingLeft={"50px"} paddingTop={"40px"}>
      <Box marginBottom={"24px"}>
        <Typography variant={"h5"}>Filters</Typography>
      </Box>
      <Divider />
      <Stack marginTop={"24px"}>
        <FormControlLabel
          style={{ justifyContent: "space-between", marginLeft: 0, marginBottom: 8 }}
          value="start"
          control={<Switch color="primary" />}
          label="Subscription Only"
          labelPlacement="start"
          checked={filterValues?.subscription.toString() === "true"}
          onChange={(e) => _handleChange("subscription", e.target.checked, e)}
        />
        <TextFilter
          id={"tags"}
          label={"Search Tags"}
          placeholder={"Cat, Dog, ..."}
          value={filterValues?.tags}
          onChange={_handleChange}
        ></TextFilter>

        <TextFilter
          id={"maximumPrice"}
          label={"Maximum Price"}
          placeholder={"Maximum Price"}
          value={filterValues?.maximumPrice}
          onChange={_handleChange}
        ></TextFilter>
      </Stack>
    </Stack>
  );
}
