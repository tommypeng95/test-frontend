import { Box, Typography } from "@material-ui/core";
import React from "react";
import ProductCollectionCard from "./Product.Collection.Card";
import Loading from "./Loading";
import LoadMoreButton from "./LoadMoreButton";

export default function ProductCollectionTable({ productData, count, isLoading, loadMore, isLoadingMore }) {
  if (isLoading)
    return (
      <Box margin={"auto"}>
        <Loading>Loading data</Loading>
      </Box>
    );

  if (!productData?.length)
    return <Box margin={"auto"}>No items could be found to match your filters, try adjusting them.</Box>;

  const isMore = productData.length < parseInt(count);

  return (
    <Box display={"flex"} flexDirection={"column"} width={"100%"}>
      <Box marginTop={"25px"} marginBottom={"15px"} alignItems={"left"} width={"100%"}>
        <Typography variant={"body2"}>{count || 0} items</Typography>
      </Box>
      <Box display={"flex"} flexWrap={"wrap"}>
        {productData.map((product) => (
          <ProductCollectionCard key={product.id} product={product}></ProductCollectionCard>
        ))}
      </Box>
      <LoadMoreButton isMore={isMore} isLoadingMore={isLoadingMore} loadMore={loadMore}></LoadMoreButton>
    </Box>
  );
}
