import React from "react";
import { TextField, Typography } from "@material-ui/core";

export default function TextFilter({ id, label, value, onChange, placeholder }) {
  return (
    <>
      <Typography variant={"body1"}>{label}</Typography>
      <TextField
        variant={"outlined"}
        size={"small"}
        placeholder={placeholder}
        value={value}
        onChange={(e) => onChange(id, e.target.value, e)}
      ></TextField>
    </>
  );
}
