import { Box, CircularProgress, Typography } from "@material-ui/core";
import React from "react";

export default function Loading({ children }) {
  return (
    <Box textAlign={"center"}>
      <CircularProgress />
      <Typography variant={"body1"}>{children}</Typography>
    </Box>
  );
}
