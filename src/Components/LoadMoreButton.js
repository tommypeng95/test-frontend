import { Box, Button } from "@material-ui/core";
import React from "react";
import Loading from "./Loading";

export default function LoadMoreButton({ isLoadingMore, isMore, loadMore }) {
  return (
    <Box display={"flex"} justifyContent={"center"} marginBottom={"30px"}>
      {isLoadingMore ? (
        <Loading>Loading more data...</Loading>
      ) : (
        isMore && (
          <Button onClick={loadMore} style={{ margin: "auto 0px" }} variant={"outlined"}>
            Load more
          </Button>
        )
      )}
    </Box>
  );
}
