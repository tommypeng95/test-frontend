import React from "react";
import { Box, Typography } from "@material-ui/core";

export default function PageLayout({ children }) {
  return (
    <Box textAlign={"left"}>
      <Box
        height={90}
        borderBottom={"1px solid #ccd2e3"}
        display={"flex"}
        alignItems={"center"}
        justifyContent={"center"}
      >
        <Typography variant="h2">PetLabCo.</Typography>
      </Box>
      <Box width={"100%"} margin={"auto"}>
        {children}
      </Box>
    </Box>
  );
}
