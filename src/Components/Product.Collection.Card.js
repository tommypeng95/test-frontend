import React from "react";
import {
  CardBase,
  CardButton,
  CardContent,
  CardPrice,
  CardThumbnail,
  CardSubscriptionBubble,
  CardTitle,
  CardTags,
} from "./Card";

export default function ProductCollectionCard({ product }) {
  const { title, image_src, url, price, subscription, tags } = product;
  return (
    <CardBase>
      <CardSubscriptionBubble
        hasSubscription={subscription}
        top={"4px"}
        right={"4px"}
        left={"4px"}
        color={"text.primary"}
        style={{ backgroundColor: "light-red" }}
      ></CardSubscriptionBubble>
      <CardThumbnail src={image_src}></CardThumbnail>
      <CardContent>
        <CardTitle>{title}</CardTitle>
        <CardTags>{tags}</CardTags>
        <CardPrice>{price}</CardPrice>
        <CardButton href={url}>SHOP NOW</CardButton>
      </CardContent>
      {/* <pre>{JSON.stringify(product, undefined, 2)}</pre> */}
    </CardBase>
  );
}
