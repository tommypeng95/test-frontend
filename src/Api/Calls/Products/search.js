import GET from "../../HttpRequests/Get";

export default async function searchProducts(data) {
  const maxPriceQuery = !!data.maximumPrice ? "&price_lte=" + data.maximumPrice : "";
  const tagsQuery = !!data.tags
    ? data.tags
        .split(" ")
        .map((tag) => "&tags_like=" + tag)
        .join("&")
    : "";
  const subscription = JSON.parse(data.subscription) ? "&subscription=" + data.subscription : "";
  const page = `&_page=${!!data?.page ? data?.page : 1}`;
  const limit = `&_limit=${!!data?.limit ? data?.limit : 4}`;
  const query = `?${maxPriceQuery}${tagsQuery}${subscription}${page}${limit}`;
  const res = await GET("/products" + query);

  //simple fetch response handling
  if (res?.status === 200) {
    return { success: true, data: res.data, count: res.headers["x-total-count"] };
  }

  //could add more cases or handle all response
  //assuming that if the status code is not 200 then it is an error.
  return { success: false, error: `${res?.status?.toString()} code: ${res.statusText}` };
}
