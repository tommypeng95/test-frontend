# SOLUTION

## Test Cases

Feature: "Load More" Paginiation

As a user, I want to see results as soon as I search for them, this means I need fast performance.

Scenario: Display Load More Button

> Given the user is on the product collection page<br />
> And the user has made a successful search<br />
> When the user reaches the end of products being displayed<br />
> And there are more results that can be loaded<br />
> Then a "Load More" button is displayed

Scenario: Load More Results

> When the user clicks the "Load More" button<br />
> Then more results are fetched from the server<br />
> And the results are added to the end of list

Feature: User Searches By Tags

As a user, I am not sure on the spelling of a word, but I want to try to search it anyway.

Scenario: Searching tags

> Given the user is on the product collection page<br />
> And can see the filters siderbar for tags<br />
> When I search for "Dog" in filters sidebar<br />
> Then I expect to see 10 products in the resulting table

Scenario: Searching tags with an incorrect search term

> Given the user is on the product collection page<br />
> When the search phrase "Doh" is entered<br />
> Then no results are displayed<br />
> And a message is displayed telling the user to try another search

Scenario: Searching tags with a partial search term

> Given the user is on the product collection page<br />
> When the search phrase "Do" is entered<br />
> Then I expect to see 10 products in the resulting table<br />
> And the results have tags that contain the search

## Estimation

Estimated: 3 hours

Spent: ~ 2 hours 45 mins

- Development: 1 hours 45 mins
- Troubleshooting: 45 mins
- Write-up: 15 mins

## Solution

Comments on your solution

### Typescript

Typically I would use Typescript, but due to the size of the project, it would add more complexity than is necessary here.

### File Heirarchy

📦src<br />
┣ 📂Api<br />
┃ ┣ 📂Calls<br />
┃ ┃ ┗ 📂Products<br />
┃ ┃ ┃ ┗ 📜search.js<br />
┃ ┣ 📂HttpRequests<br />
┃ ┃ ┗ 📜Get.js<br />
┃ ┗ 📜API_BASE_URI.js<br />
┣ 📂Components<br />
┣ 📂Hooks<br />
┣ 📂Pages<br />
┣ 📂Reducers<br />
┃ ┗ 📂arrayReducer<br />
┃ ┃ ┣ 📜actions.js<br />
┃ ┃ ┣ 📜arrayReducer.js<br />
┃ ┃ ┗ 📜index.js<br />
┗ 📂utils<br />

The components, hooks and pages are self explanatory.

#### API Folder

I have split the Api folder into Calls and HTTPRequests, this means I can create functions for each request type and then use them in calls. The calls file is organised depending on the api extension, ie calls to /products/\* are in the products folder.

The API_BASE_URI.js file would typically be in a .env file, but this could be easily upgraded in the future.

#### Reducer Folder

Within the Reducers folder, I have included an reducer hierarchy example. This would is the reducer itself and the actions that can be taken on the reducer. This is just a good pattern to follow ensure it is maintainable and mistakes aren't made.

### Filtering

There are three filter inputs, these are all found in the left sidebar. The filters are stored in the url rather than in state, this allows user to share their links with friends/family with deterministic results.

### Debounced calls

When the user firsts loads the page it updates products immediately, using a useEffect.

Future reloads are triggered through a debounced fetch, which is called when any filter is changed. The user is show a loading screen until the call is completed, but this improves performance by reducing the number of server calls in the case where the user makes multiple changes.

### Pagination

I wanted to use a cursor based pagination, but it is beyond the scope of this task. I opted to emulate the "Load More" button that is found on PetLabCo.com. I chose a small number of products to fetched each time, purely to demonstrate the pagination. On a live site I would fetch more products each time.

When more products are loaded, they are appended to the end of the current product list.

### Page Resizing

The page is partially optimised to work on multiple screen sizes, but I did not spend time to make it work well on mobile.

I could make further improvements to ensure the products fill the page, but I feel they are displayed well for the purposes of this challenge.

### Material UI

I elected to use material UI to assist in the development of components.

### Improvemnts

- Include a cursor and a hasMore field from the backend, and include the cursor in the url.
- I could consider using a slider instead of input box for pricing fields.
- Add more filter options, such as title search, and a sortby option
- Add a "quick buy" or "add to basket" option from the product card.
- If there is no products to be displayed, offer some other products they may be interested as well as a message.
- Index backened entities and use a fuzzy search to offer better results faster.
